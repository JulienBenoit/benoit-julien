﻿using Interface_SAGE.Core;
using Interface_SAGE.Helper;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Interface_SAGE.Forms
{
    public partial class FormParam : Form
    {
        #region propriete
        MainProgram Core;
        private String Tmp_FicConversionWOO = "";
        private String Tmp_FicConversionFutur = "";
        private DateTime Current_Date;
        private DateTime Current_Date2;
        #endregion propriete

        #region init and load
        public FormParam(MainProgram core)
        {
            Core = core;
            InitializeComponent();

            ckPwd.Checked = false;
            ckBox2.Checked = false;
            cbFTPProtocol.Items.Add("FTP");
            cbFTPProtocol.Items.Add("SFTP");
        }
        private void FormParam_Load(object sender, EventArgs e)
        {
            try
            {
                ParamToForm();
                if (Core.baseSage.IsOpen == false)
                    connexion();
                else if (Core.baseSage.IsOpen)
                {
                    // indication de connexion à l'utilisateur
                    lblStatus.Text = "Connecté Base : " + Core.baseSage.Nom_Dossier; 
                }
                else
                    Commun.Journalise("Connexion", "Echec de Connexion à la base " + Core.baseSage.Nom_Dossier);
            }
            catch (Exception E)
            { MessageBox.Show("Erreur au chargement : " + E.Message); }

            toolStripStatusLabel2.Text = "Fichier Paramètres : " + Core.GetCheminFichierParametres;
        }
        private void connexion()
        {
            lblStatus.Text = "Déconnecté";
            if (String.IsNullOrEmpty(Core.GetParametres.Sage_Raccourci) == false)
            {
                // Connexion à SAGE
                Core.Connect_Sage();


                if (Core.baseSage.IsOpen)
                {
                    // indication de connexion à l'utilisateur
                    lblStatus.Text = "Connecté Base : " + Core.baseSage.Nom_Dossier;
                    //Commun.Journalise("Connexion", "Connexion à la base " + Core.baseSage.Nom_Dossier + " établie.");

                }
                else
                    Commun.Journalise("Connexion", "Echec de Connexion à la base " + Core.baseSage.Nom_Dossier);
            }
            toolStripStatusLabel2.Text = "Fichier Paramètres : " + Core.GetCheminFichierParametres;
        }
        #endregion init and load

        #region gestion paramètres
        private void FormToPAram()
        {
            // Sage
            Core.GetParametres.GratuitRemise = rbRem100.Checked;
            Core.GetParametres.Prod = ! ckProd.Checked ;
            if (String.IsNullOrEmpty(PwBox.Text) == false | ckPwd.Checked == true)
                Core.GetParametres.Sage_MdpCripte = DataProtection.Protect(PwBox.Text);
            Core.GetParametres.Sage_Raccourci = txtBoxRaccourci.Text;
            Core.GetParametres.Sage_Utilisateur = txtUser.Text;

            Core.GetParametres.WooConversion = Tmp_FicConversionWOO;
            Core.GetParametres.FuturConversion = Tmp_FicConversionFutur;

            // WooCommerce
            Core.GetParametres.WOO_WorkDir = txWOO_WorkDir.Text;
            Core.GetParametres.WOO_ExportDir = txWOO_Exportdir.Text;
            Core.GetParametres.WOO_DataSep = txWOO_DataSep.Text;
            Core.GetParametres.WOO_DataDel = txWOO_DataDel.Text;
            Core.GetParametres.WOO_DocFicNomORDER = txWOO_FicNomORDER.Text;
            Core.GetParametres.WOO_DocFicNomPRODUCT = txWOO_FicNomPRODUCT.Text;
            Core.GetParametres.WOO_Ligne_entete = ckWOO_Entete.Checked;
            Core.GetParametres.WOO_DocSouche = txtSoucheNewDoc.Text;
           // Core.GetParametres.WOO_RemisePanier = "";
            Core.GetParametres.WOO_DocLibr_Origine_commande = txWOO_Libr_OrigCde.Text;
            Core.GetParametres.WOO_DocLibr_Comment_LIV = txWOO_Libr_CommentLIV.Text;
            Core.GetParametres.WOO_ClientLibr_ID = txWOO_IDClient.Text;
            Core.GetParametres.WOO_Libr_Horodatage = txWOO_Libr_Horo.Text;
            // Core.GetParametres.FormatDate = txtFormatDate.Text;
            Core.GetParametres.PrefixClient = txtPrefixClient.Text;

            Core.GetParametres.WOO_APIURL = txtWOO_APIUrl.Text;
            Core.GetParametres.WOO_APIlogin = txtWOO_APILogin.Text;
            Core.GetParametres.WOO_APIMdp = DataProtection.Protect(txtWOO_APIPass.Text);

            // Catégorie de client
            Core.GetParametres.WOO_StatClient= txStatClient.Text;
            Core.GetParametres.WOO_Stat1 = txStat1.Text;
            Core.GetParametres.WOO_Stat2 = txStat2.Text;
            Core.GetParametres.WOO_Stat3 = txStat3.Text;
            Core.GetParametres.WOO_StatTarif1 = txCatTarif1.Text;
            Core.GetParametres.WOO_StatTarif2 = txCatTarif2.Text;
            Core.GetParametres.WOO_StatTarif3 = txCatTarif3.Text;
            Core.GetParametres.WOO_StatCpta1  = txCatCompta1.Text;
            Core.GetParametres.WOO_StatCpta2  = txCatCompta2.Text;
            Core.GetParametres.WOO_StatCpta3  = txCatCompta3.Text;
            Core.GetParametres.WOO_StatCptG1 = txCompteCol1.Text;
            Core.GetParametres.WOO_StatCptG2 = txCompteCol1.Text;
            Core.GetParametres.WOO_StatCptG3 = txCompteCol1.Text;
            Core.GetParametres.WOO_StatPayeur1= txTiersPayeur1.Text;
            Core.GetParametres.WOO_StatPayeur2= txTiersPayeur2.Text;
            Core.GetParametres.WOO_StatPayeur3= txTiersPayeur3.Text;
            Core.GetParametres.WOO_ISOFR= txWOO_ISOFrance.Text;
            Core.GetParametres.WOO_CptaUE= txWOO_CptaUE.Text;
            Core.GetParametres.WOO_CptaDOM= txWOO_CptaDOM.Text;
            Core.GetParametres.WOO_CptaAutre= txWOO_CptaAutres.Text;

            // FuturLOG
            Core.GetParametres.Futur_APIURI = txtFuturAPIURL.Text;
            Core.GetParametres.Futur_APIMerchantcode = txtFuturAPIMerchantCode.Text;
            Core.GetParametres.Futur_APILogin = txtFuturAPILogin.Text;
            if (String.IsNullOrEmpty(txtFuturAPIKey.Text) == false)
                Core.GetParametres.Futur_APIKey = DataProtection.Protect(txtFuturAPIKey.Text);
            Core.GetParametres.Futur_VentesSend = txtFuturVentesSend.Text;
            Core.GetParametres.Futur_VendesGet = txtFuturVentesGet.Text;
            Core.GetParametres.Futur_AchatsSend = txtFuturAchatSend.Text;
            Core.GetParametres.Futur_AchatGet = txtFuturAchatGet.Text;

            Core.GetParametres.Futur_StockGet = txtFuturStockGet.Text;
            Core.GetParametres.Futur_ProduitSend = txtFuturProductSend.Text;
            Core.GetParametres.Futur_ListSoucheExport = txtFuturListSouche.Text;
            Core.GetParametres.Futur_Libr_Horodatage = txtFuturHoro.Text;
            Core.GetParametres.Futur_Exportconfirme = ckExportConfirme.Checked;
            Core.GetParametres.Futur_Exportcomposants = ckExportComposant.Checked;
            Core.GetParametres.Futur_Depot = txtFuturDepot.Text;

            // Acces FTP
            Core.GetParametres.FTPUri = txtFTPURI.Text;
            Core.GetParametres.FTPProtocol = (String)cbFTPProtocol.SelectedItem;
            Core.GetParametres.FTPPort = txtFTPPort.Text;
            Core.GetParametres.FTPSSH = txtFTPSSH.Text;
            Core.GetParametres.FTPUser = txtFTPUser.Text;
            if (String.IsNullOrEmpty(txtFTPMdp.Text) == false)
                Core.GetParametres.FTP_MdpCripte = DataProtection.Protect(txtFTPMdp.Text);
            Core.GetParametres.FTPPathOrder= txtFTPPathOrder.Text;
            Core.GetParametres.FTPPathProduct= txtFTPPathProduct.Text;

            // interface EDI



            // Si date dernier export à changer ( forcer par l'utilisateur,) sauvegarde dans le fichier config.
            try
            {
                if (Current_Date.Equals(DateTime.Parse(txDernImport_Liv.Text)) == false)
                {
                    Core.GetParametres.lastImport_Liv= DateTime.Parse(txDernImport_Liv.Text);
                    MessageBox.Show("Le changement de \"date de dernier import livraison\" ne sera pris en compte qu'après avoir redémarré le programme.", "INFO", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            catch (Exception E)
            { MessageBox.Show("Erreur écriture fichier config : " + E.Message); }

            // Si date dernier export à changer ( forcer par l'utilisateur,) sauvegarde dans le fichier config.
            try
            {
                if (Current_Date2.Equals(DateTime.Parse(txDernImport_Rec.Text)) == false)
                {
                    Core.GetParametres.lastImport_Recept= DateTime.Parse(txDernImport_Rec.Text);
                    MessageBox.Show("Le changement de \"date de dernier import réception\" ne sera pris en compte qu'après avoir redémarré le programme.", "INFO", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            catch (Exception E)
            { MessageBox.Show("Erreur écriture fichier config : " + E.Message); }

        }


       
        
        private void ParamToForm()
        {
            try
            {
                // Chargement des paramètres ( A l'exception des Mdp )
                // Sage
                ckProd.Checked = !Core.GetParametres.Prod;
                rbRem100.Checked = Core.GetParametres.GratuitRemise;
                rbNoValeur.Checked = !Core.GetParametres.GratuitRemise;
                txtBoxRaccourci.Text = Core.GetParametres.Sage_Raccourci;
                txtUser.Text = Core.GetParametres.Sage_Utilisateur;

                Tmp_FicConversionWOO= Core.GetParametres.WooConversion;
                Tmp_FicConversionFutur = Core.GetParametres.FuturConversion;

                // WooCommerce
                txWOO_WorkDir.Text = Core.GetParametres.WOO_WorkDir;
                txWOO_Exportdir.Text = Core.GetParametres.WOO_ExportDir;
                txWOO_DataSep.Text = Core.GetParametres.WOO_DataSep;
                txWOO_DataDel.Text = Core.GetParametres.WOO_DataDel;
                txWOO_FicNomORDER.Text = Core.GetParametres.WOO_DocFicNomORDER;
                txWOO_FicNomPRODUCT.Text = Core.GetParametres.WOO_DocFicNomPRODUCT;
                ckWOO_Entete.Checked = Core.GetParametres.WOO_Ligne_entete;
                txtSoucheNewDoc.Text = Core.GetParametres.WOO_DocSouche;
                // Core.GetParametres.WOO_RemisePanier = "";
                txWOO_Libr_OrigCde.Text = Core.GetParametres.WOO_DocLibr_Origine_commande;
                txWOO_Libr_CommentLIV.Text = Core.GetParametres.WOO_DocLibr_Comment_LIV ;
                txWOO_IDClient.Text = Core.GetParametres.WOO_ClientLibr_ID ;
                txWOO_Libr_Horo.Text = Core.GetParametres.WOO_Libr_Horodatage;
                //txtFormatDate.Text = Core.GetParametres.FormatDate ;
                txtPrefixClient.Text = Core.GetParametres.PrefixClient ;

                txtWOO_APIUrl.Text = Core.GetParametres.WOO_APIURL;
                txtWOO_APILogin.Text = Core.GetParametres.WOO_APIlogin;
                txtWOO_APIPass.Text = "";

                // Catégorie de client
                txStatClient.Text = Core.GetParametres.WOO_StatClient ;
                txStat1.Text = Core.GetParametres.WOO_Stat1;
                txStat2.Text = Core.GetParametres.WOO_Stat2;
                txStat3.Text = Core.GetParametres.WOO_Stat3;    
                txCatTarif1.Text = Core.GetParametres.WOO_StatTarif1 ;
                txCatTarif2.Text = Core.GetParametres.WOO_StatTarif2 ;
                txCatTarif3.Text = Core.GetParametres.WOO_StatTarif3 ;
                txCatCompta1.Text = Core.GetParametres.WOO_StatCpta1 ;
                txCatCompta2.Text = Core.GetParametres.WOO_StatCpta2 ;
                txCatCompta3.Text = Core.GetParametres.WOO_StatCpta3 ;
                txCompteCol1.Text = Core.GetParametres.WOO_StatCptG1 ;
                txCompteCol2.Text = Core.GetParametres.WOO_StatCptG2 ;
                txCompteCol3.Text = Core.GetParametres.WOO_StatCptG3 ;
                txTiersPayeur1.Text = Core.GetParametres.WOO_StatPayeur1;
                txTiersPayeur2.Text = Core.GetParametres.WOO_StatPayeur2;
                txTiersPayeur3.Text = Core.GetParametres.WOO_StatPayeur3;
                txWOO_ISOFrance.Text= Core.GetParametres.WOO_ISOFR     ;
                txWOO_CptaUE.Text= Core.GetParametres.WOO_CptaUE   ;
                txWOO_CptaDOM.Text= Core.GetParametres.WOO_CptaDOM   ;
                txWOO_CptaAutres.Text= Core.GetParametres.WOO_CptaAutre ;

                // FuturLOG
                txtFuturAPIURL.Text = Core.GetParametres.Futur_APIURI ;
                txtFuturAPIMerchantCode.Text = Core.GetParametres.Futur_APIMerchantcode;
                txtFuturAPILogin.Text = Core.GetParametres.Futur_APILogin;
                txtFuturVentesSend.Text = Core.GetParametres.Futur_VentesSend;
                txtFuturVentesGet.Text = Core.GetParametres.Futur_VendesGet;
                txtFuturAchatSend.Text = Core.GetParametres.Futur_AchatsSend;
                txtFuturAchatGet.Text = Core.GetParametres.Futur_AchatGet;

                txtFuturStockGet.Text = Core.GetParametres.Futur_StockGet;
                txtFuturProductSend.Text = Core.GetParametres.Futur_ProduitSend;
                txtFuturListSouche.Text = Core.GetParametres.Futur_ListSoucheExport;
                txtFuturHoro.Text = Core.GetParametres.Futur_Libr_Horodatage;
                ckExportConfirme.Checked = Core.GetParametres.Futur_Exportconfirme;
                ckExportComposant.Checked = Core.GetParametres.Futur_Exportcomposants;
                txtFuturDepot.Text = Core.GetParametres.Futur_Depot;

                // Acces FTP
                txtFTPURI.Text = Core.GetParametres.FTPUri;
                cbFTPProtocol.SelectedItem = Core.GetParametres.FTPProtocol;
                txtFTPPort.Text = Core.GetParametres.FTPPort;
                txtFTPSSH.Text = Core.GetParametres.FTPSSH;
                txtFTPUser.Text = Core.GetParametres.FTPUser;
                txtFTPMdp.Text = "";
                txtFTPPathOrder.Text = Core.GetParametres.FTPPathOrder;
                txtFTPPathProduct.Text = Core.GetParametres.FTPPathProduct;

                // interface EDI



                // chargement ddate dernier export dans fichier config
                try
                {
                    Current_Date = Core.GetParametres.lastImport_Liv;
                    txDernImport_Liv.Text = Current_Date.ToString();
                }
                catch (Exception E)
                { MessageBox.Show("Erreur Lecture fichier config : " + E.Message); }
                try
                {
                    Current_Date2 = Core.GetParametres.lastImport_Recept;
                    txDernImport_Rec.Text = Current_Date2.ToString();
                }
                catch (Exception E)
                { MessageBox.Show("Erreur Lecture fichier config : " + E.Message); }

                if (String.IsNullOrEmpty(Tmp_FicConversionFutur) == false & File.Exists(Tmp_FicConversionFutur) == false)
                    File.Create(Tmp_FicConversionFutur);
                if (String.IsNullOrEmpty(Tmp_FicConversionWOO) == false &  File.Exists(Tmp_FicConversionWOO) == false)
                    File.Create(Tmp_FicConversionWOO);

                PwBox.Text = "";
            }
            catch (KeyNotFoundException)
            { MessageBox.Show("Impossible d'afficher les paramètres, le format du fichier est incorrect");  }

        }
        #endregion gestion paramètres

        #region boutons et menus
        private void chargerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dialogsel = new OpenFileDialog())
            {
                dialogsel.Reset();
                dialogsel.Title = "Chargement du fichier de paramètres";
                dialogsel.FileName = Core.GetCheminFichierParametres;
                dialogsel.Filter = "Ficher Json (*.json)|*.json|Fichier XML (*.xml)|*.xml|Tout fichier|*.*";
                dialogsel.FilterIndex = 0;
                if (dialogsel.ShowDialog() == DialogResult.OK)
                {
                    Core.LoadParam(dialogsel.FileName);
                    ParamToForm();
                    connexion();
                }
                dialogsel.Dispose();
            }
        }

        private void sauvegarderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (SaveFileDialog dialogsel = new SaveFileDialog())
            {
                dialogsel.Reset();
                dialogsel.Title = "Enregistrement du fichier de paramètres ";
                dialogsel.FileName = Core.GetCheminFichierParametres;
                dialogsel.Filter = "Ficher Json (*.json)|*.json|Fichier XML (*.xml)|*.xml|Tout fichier|*.*";
                dialogsel.FilterIndex = 1;
                if (dialogsel.ShowDialog() == DialogResult.OK)
                {
                    FileInfo fic = new FileInfo(dialogsel.FileName);

                    FormToPAram();
                    Core.SaveParam(fic);
                    connexion();
                }
                dialogsel.Dispose();
            }
        }

        private void BtConversion_Click(object sender, EventArgs e)
        {
            Forms.FormConversion formconv = new Forms.FormConversion(ref Tmp_FicConversionFutur, this.Core);
            formconv.ShowDialog(this);
            Tmp_FicConversionWOO = Core.GetParametres.WooConversion;
        }

        private void BtConversion2_Click(object sender, EventArgs e)
        {
            Forms.FormConversion formconv = new Forms.FormConversion(ref Tmp_FicConversionWOO, this.Core);
            formconv.ShowDialog(this);
            Tmp_FicConversionFutur = Core.GetParametres.FuturConversion;
        }

        private void BtAnnul_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtOk_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == MessageBox.Show("Les modifications seront pris en compte, même sans enregistrer ?", "Confirmer", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2))
            {
                FormToPAram();
                this.Close();
            }
        }

        #endregion boutons et menus

        #region Bouton Dialog
        private void btSelAppli_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dialogsel = new OpenFileDialog())
            {
                dialogsel.Reset();
                dialogsel.Title = "Sélectionner le raccourci de la Gestion commercial SAGE";
                if (String.IsNullOrEmpty(txtBoxRaccourci.Text) == false)
                    dialogsel.FileName = txtBoxRaccourci.Text;
                else
                    dialogsel.FileName = "*.GCM";
                dialogsel.Filter = "Base Commerciale (*.gcm)|*.GCM|Tous les fichiers (*.*)|*.*";
                dialogsel.FilterIndex = 0;
                if (dialogsel.ShowDialog() == DialogResult.OK)
                    txtBoxRaccourci.Text = dialogsel.FileName;
                dialogsel.Dispose();
            }
        }

       
        #endregion Bouton Dialog


        private void txDernImport_Liv_Validating(object sender, CancelEventArgs e)
        {
            if ( DateTime.TryParse(txDernImport_Liv.Text, out DateTime dummydate) == false)
            {
                MessageBox.Show("Saisie incorrecte !", "ERREUR");
                e.Cancel = true;
            }
        }
        private void txDernImport_Rec_Validating(object sender, CancelEventArgs e)
        {
            if (DateTime.TryParse(txDernImport_Rec.Text, out DateTime dummydate) == false)
            {
                MessageBox.Show("Saisie incorrecte !", "ERREUR");
                e.Cancel = true;
            }
        }

        private void btXML_TaskPostStatutDoc_Click(object sender, EventArgs e)
        {
            //Planif.Create_PostStatutDoc(Core.Get_cheminFichierParametres(), 0, 5, 7, 30, 12);
            Helper.Planif.Create_XML_To_Import("Post_Statuts_DOC_SAGE", Core.GetCheminFichierParametres, 0, 5, 7, 30, 12);
        }


        private void txtBoxRaccourci_TextChanged(object sender, EventArgs e)
        {
            connexion();
        }

        private void btWOO_WorkDir_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialog dialogsel = new FolderBrowserDialog())
            {
                dialogsel.Reset();
                dialogsel.Description = "Sélectionner le répertoire de travail du programme";
                if (String.IsNullOrEmpty(txWOO_WorkDir.Text) == false)
                    dialogsel.SelectedPath = Path.GetPathRoot(txWOO_WorkDir.Text);
                else
                    dialogsel.SelectedPath = "";
                if (dialogsel.ShowDialog() == DialogResult.OK)
                    txWOO_WorkDir.Text = dialogsel.SelectedPath;
                dialogsel.Dispose();
            }
        }

        private void btWOO_ExportDir_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialog dialogsel = new FolderBrowserDialog())
            {
                dialogsel.Reset();
                dialogsel.Description = "Sélectionner le répertoire d'export du programme";
                if (String.IsNullOrEmpty(txWOO_Exportdir.Text) == false)
                    dialogsel.SelectedPath = Path.GetPathRoot(txWOO_Exportdir.Text);
                else
                    dialogsel.SelectedPath = "";
                if (dialogsel.ShowDialog() == DialogResult.OK)
                    txWOO_Exportdir.Text = dialogsel.SelectedPath;
                dialogsel.Dispose();
            }
        }
    }
}
