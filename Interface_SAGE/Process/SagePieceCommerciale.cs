﻿using Interface_SAGE.Helper;
using Objets100cLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Interface_SAGE.Process
{
    public  class PieceCommerciale
    {

        private BSCIALApplication100c om_BaseCial;

        private IPMDocument om_ProcDoc;
        private Boolean started;
        private Boolean finished;

        public PieceCommerciale(BSCIALApplication100c _om_BaseCial,DocumentType doctype)

        {
            om_BaseCial = _om_BaseCial;
            om_ProcDoc = om_BaseCial.CreateProcess_Document(doctype);
            started = false;
            finished = false;
        }
        public static TextBox TextBoxMessage { get; set; }
        public IBODocumentVente3 CreePieceVente( String codeClient, DateTime date, String refPiece ="", String Do_piece ="", String Souche = "")
        {
            if ( started == true )
                throw new Exception("Erreur le process a déjà été initialisé.");
            
            try
            {
                started = true;
                // Récupération de l'entête
                IBODocumentVente3 entete = om_ProcDoc.Document as IBODocumentVente3;
                
                // souche
                if (String.IsNullOrEmpty(Souche) == false)
                    entete.Souche = om_BaseCial.FactorySoucheVente.ReadIntitule(Souche);
                
                // Numéro pièce
                if (String.IsNullOrEmpty(Do_piece) == false)
                    entete.DO_Piece = Do_piece;
                else
                    entete.SetDefaultDO_Piece();

                // Définition du client
                entete.SetDefaultClient(om_BaseCial.CptaApplication.FactoryClient.ReadNumero(codeClient));

                entete.DO_Date = date;

                entete.DO_Ref = refPiece.Substring(0, Math.Min(17, refPiece.Length));
                if (refPiece.Length > 17)
                    Commun.Journalise("CreePieceCommerciale", "Document " + refPiece + " du " + date.ToString("dd/MM/yyyy") + " : La référence sera tronquée.", TextBoxMessage);
                    
                
                
                entete.Write();

                return entete;
            }
            catch (Exception ex)
            { throw new Exception("Erreur en création du document " + refPiece + " : " + ex.Message); }

        }
        public IBODocumentVenteLigne3 AjouteLigneSpeciale( String Reference, Double quantite, Double remiseP = 0, Double remiseU = 0, Double remiseF = 0, Double? PUht = null, Double? PUTTC = null, Boolean IsRemiseExceptionnelle = false, Boolean IsRemisePied = false, Boolean Add_Component_After = true)
        {
            if (started == false)
                throw new Exception("Erreur le process n'est pas initialisé.");
            if (finished == true)
                throw new Exception("Erreur le process est déjà terminé.");

            String remise = "";
            try
            {
                IBODocumentVenteLigne3 ligne = om_ProcDoc.AddArticleReference(Reference, quantite) as IBODocumentVenteLigne3;

               if (PUht != null)
                { 
                    ligne.DL_PrixUnitaire = (double)PUht;
                    ligne.TTC = false;
                  }
                if(PUTTC != null)
                {
                    ligne.DL_PUTTC= (double)PUTTC;
                    ligne.TTC = true;
                }

                if (remiseP == 0 & remiseU == 0 & remiseF == 0)
                    ligne.SetDefaultRemise();
                else
                {
                   
                    if (remiseP != 0)
                        remise += remiseP.ToString() + "%";
                    if ( remiseU != 0)
                    {
                        remise = remise == "" ? "" : remise + " + ";
                        remise += remiseU.ToString() + "U";
                    }
                    if (remiseF != 0)
                    {
                        remise = remise == "" ? "" : remise + " + ";
                        remise += remiseF.ToString() + "F";
                    }
                    ligne.Remise.FromString(remise);
                }
                ligne.IsRemiseExceptionnelle = IsRemiseExceptionnelle;
                ligne.IsRemisePied= IsRemisePied;
               
                /*if (Add_Component_After == true)
                {
                    // Add information of Bundle if exist
                    if (om_BaseCial.FactoryArticle.ReadReference(Reference).FactoryArticleNomenclature.List.Count > 0)
                        ligne.ArticleCompose = om_BaseCial.FactoryArticle.ReadReference(Reference);

                    // Write bundle line in all case
                    ligne.WriteDefault();


                    foreach (IBOArticleNomenclature3 mIboArt in om_BaseCial.FactoryArticle.ReadReference(Reference).FactoryArticleNomenclature.List)
                    {
                        IBODocumentVenteLigne3 DocLigneComposant = null;
                        //  Add line by line the component of the bundle
                        if (mIboArt.NO_Type == ComposantType.ComposantTypeVariable)
                            DocLigneComposant = om_ProcDoc.AddArticleReference(mIboArt.ArticleComposant.AR_Ref, mIboArt.NO_Qte * ligne.DL_Qte) as IBODocumentVenteLigne3;
                        else
                            DocLigneComposant = om_ProcDoc.AddArticleReference(mIboArt.ArticleComposant.AR_Ref, mIboArt.NO_Qte) as IBODocumentVenteLigne3;

                        ///* TODO Not supported :
                         //  Dépôt
                         //  Gamme
                         //  Opération
                         //  Répartition
                         ///
                        DocLigneComposant.ArticleCompose = mIboArt.Article;
                        if (remiseP == 0 & remiseU == 0 & remiseF == 0)
                              DocLigneComposant.SetDefaultRemise();
                        else
                            DocLigneComposant.Remise.FromString(remise);

                        DocLigneComposant.WriteDefault();
                    }
                }
                else
                {
                    // no component supported, simply write product line
                    ligne.Write();
                }*/

                ligne.Write();

                return ligne;
            }
            catch (Exception ex)
            { throw new Exception("Erreur en ajout de ligne de ventes " + Reference + " à la pièce " + om_ProcDoc.Document.DO_Ref + " : " + ex.Message); }

        }
        public IBODocumentVenteLigne3 AjouteLigneProduitBefore(String Reference, Double quantite,String Designation, Double remise, IBODocumentVenteLigne3 LigneRedevance = null , Double PUTTC = 0)
        {
            if (started == false)
                throw new Exception("Erreur le process n'est pas initialisé.");
            if (finished == true)
                throw new Exception("Erreur le process est déjà terminé.");

            try
            {
                IBODocumentVenteLigne3 ligne = om_ProcDoc.AddArticleReference(Reference, quantite) as IBODocumentVenteLigne3;

                if ( String.IsNullOrEmpty(Designation) == false )
                ligne.DL_Design = Designation;
                if ( PUTTC != 0 )
                {
                    ligne.TTC = true;
                    ligne.DL_PUTTC = PUTTC;
                }
               
                if (remise == 0)
                    ligne.SetDefaultRemise();
                else
                {
                    ligne.Remise.FromString(remise.ToString() + "%");
                }
                if (LigneRedevance != null)
                    ligne.WriteDefaultBefore(LigneRedevance);
                else
                    ligne.WriteDefault();

                return ligne;
            }
            catch (Exception ex)
            { throw new Exception("Erreur en ajout de ligne de ventes " + Reference + " à la pièce " + om_ProcDoc.Document.DO_Ref + " : " + ex.Message); }

        }

        public IBODocumentVenteLigne3 AjouteLigne(String Reference, Double quantite, Double remise, Double PUht = 0, Double PUttc = 0)
        {
            if (started == false)
                throw new Exception("Erreur le process n'est pas initialisé.");
            if (finished == true)
                throw new Exception("Erreur le process est déjà terminé.");

            try
            {
                IBODocumentVenteLigne3 ligne = om_ProcDoc.AddArticleReference(Reference, quantite) as IBODocumentVenteLigne3;

                if (PUht != 0)
                    ligne.DL_PrixUnitaire = PUht;
                else if (PUttc !=0)
                {
                    ligne.TTC = true;
                    ligne.DL_PUTTC = PUttc;
                }
                if (remise == 0)
                    ligne.SetDefaultRemise();
                else
                {
                    ligne.Remise.FromString(remise.ToString() + "%");
                }
                ligne.Write();

                return ligne;
            }
            catch (Exception ex)
            { throw new Exception("Erreur en ajout de ligne de ventes " + Reference + " à la pièce " + om_ProcDoc.Document.DO_Ref + " : " + ex.Message); }

        }

        /// <summary>
        /// Intégration d'une pièce comptable
        /// </summary>
        /// <param name="om_PieceComptable">Process des OM pour intégration écriture comptables</param>
        /// <param name="simulation">Si True, seulement le CANPROCESS est lancé</param>
        public IBODocument3 ValideDocCommerciale(Boolean simulation)
        {
            if (started == false)
                throw new Exception("Erreur le process n'est pas initialisé.");
            if (finished == true)
                throw new Exception("Erreur le process est déjà terminé.");

            try
            {
                if (om_ProcDoc.CanProcess)
                {
                    if (simulation == false)
                    {
                        om_ProcDoc.Process();
                        finished = true;
                        return om_ProcDoc.DocumentResult;
                    }
                }
                else
                    throw new Exception("Validation du document commercial impossible : ");
            }
            catch (Exception ex)
            {
                StringBuilder erreurs = new StringBuilder(ex.Message);

                foreach (IFailInfo om_Erreur in om_ProcDoc.Errors)
                    erreurs.AppendLine(om_Erreur.Text);

                Commun.Journalise("ValideDocCommerciale", "Erreurs du process de création : " + erreurs.ToString(), TextBoxMessage);
                throw new Exception(erreurs.ToString());
            }
            return null;
        }
    }

    /// <summary>
    /// Classe WRAPPER pour mémoriser les informations analytique pour un document non persistant
    /// </summary>

    public class oDoc
    {
        public IBODocumentVente3 Doc;
        public IBIValues Liste_Libre;
        public List<oLig> liste_Ligne;

        public oDoc()
        {
            liste_Ligne = new List<oLig>();
        }
        public oDoc(oLig ligne)
        {
            liste_Ligne = new List<oLig>();
            liste_Ligne.Add(ligne);
        }
        public Object Get_CustomField(String name)
        {
            if (this.Doc.IsPersistant == false)
            {
                // get value from wrapped list
                foreach (IBIField val in Doc.FactoryDocument.InfoLibreFields)
                {
                    switch (val.type)
                        {
                            case FieldType.FieldTypeChaine:
                                return (String)Liste_Libre[val.Name];


                            case FieldType.FieldTypeDate:
                            case FieldType.FieldTypeLDate:
                                return (DateTime)Liste_Libre[val.Name];

                            case FieldType.FieldTypeDouble:
                            case FieldType.FieldTypeMontant:
                                return (Double)Liste_Libre[val.Name];

                            case FieldType.FieldTypeFloat:
                                return (float)Liste_Libre[val.Name];

                            default:
                                return Liste_Libre[val.Name];
                        }
                }
            }
            else
            {
                // get from persistant object
                foreach (IBIField val in Doc.FactoryDocument.InfoLibreFields)
                {
                    if (val.Name == name)
                        switch (val.type)
                        {
                            case FieldType.FieldTypeChaine:
                                return (String)Doc.InfoLibre[val.Name];


                            case FieldType.FieldTypeDate:
                            case FieldType.FieldTypeLDate:
                                return (DateTime)Doc.InfoLibre[val.Name];

                            case FieldType.FieldTypeDouble:
                            case FieldType.FieldTypeMontant:
                                return (Double)Doc.InfoLibre[val.Name];

                            case FieldType.FieldTypeFloat:
                                return (float)Doc.InfoLibre[val.Name];

                            default:
                                return Doc.InfoLibre[val.Name];
                        }
                }
            }
            return null;
        }

        public void Set_CustomField(String name, object value)
        {
            if (this.Doc.IsPersistant == false)
            {
                // get value from wrapped list
                foreach (IBIField val in Doc.FactoryDocument.InfoLibreFields)
                    if (val.Name == name)
                    {
                        try
                        {
                            Liste_Libre[val.Name] = value;
                        }
                        catch { throw new Exception("Invalid Format Custom field : " + val.Name); }
                    }
            }
            else
            {
                // get from persistant object
                foreach (IBIField val in Doc.FactoryDocument.InfoLibreFields)
                    if (val.Name == name)
                    {
                        try
                        {
                            Doc.InfoLibre[val.Name] = value;
                        }
                        catch { throw new Exception("Invalid Format Custom field : " + val.Name); }
                    }
            }
        }

    }

    /// <summary>
    /// Classe WRAPPER pour mémoriser les lignes de documents non persistant
    /// </summary>

    public class oLig
    {
        public IBODocumentVenteLigne3 oLigne;
        public IBIValues Liste_Libre;
        


        public Object Get_CustomField(String name)
        {
            if (this.oLigne.IsPersistant == false)
            {
                // get value from wrapped list
                foreach (IBIField val in oLigne.FactoryDocumentLigne.InfoLibreFields)
                {
                    switch (val.type)
                    {
                        case FieldType.FieldTypeChaine:
                            return (String)Liste_Libre[val.Name];


                        case FieldType.FieldTypeDate:
                        case FieldType.FieldTypeLDate:
                            return (DateTime)Liste_Libre[val.Name];

                        case FieldType.FieldTypeDouble:
                        case FieldType.FieldTypeMontant:
                            return (Double)Liste_Libre[val.Name];

                        case FieldType.FieldTypeFloat:
                            return (float)Liste_Libre[val.Name];

                        default:
                            return Liste_Libre[val.Name];
                    }
                }
            }
            else
            {
                // get from persistant object
                foreach (IBIField val in oLigne.FactoryDocumentLigne.InfoLibreFields)
                {
                    if (val.Name == name)
                        switch (val.type)
                        {
                            case FieldType.FieldTypeChaine:
                                return (String)oLigne.InfoLibre[val.Name];


                            case FieldType.FieldTypeDate:
                            case FieldType.FieldTypeLDate:
                                return (DateTime)oLigne.InfoLibre[val.Name];

                            case FieldType.FieldTypeDouble:
                            case FieldType.FieldTypeMontant:
                                return (Double)oLigne.InfoLibre[val.Name];

                            case FieldType.FieldTypeFloat:
                                return (float)oLigne.InfoLibre[val.Name];

                            default:
                                return oLigne.InfoLibre[val.Name];
                        }
                }
            }
            return null;
        }

        public void Set_CustomField(String name, object value)
        {
            if (this.oLigne.IsPersistant == false)
            {
                // get value from wrapped list
                foreach (IBIField val in oLigne.FactoryDocumentLigne.InfoLibreFields)
                    if (val.Name == name)
                    {
                        try
                        {
                            Liste_Libre[val.Name] = value;
                        }
                        catch { throw new Exception("Invalid Format Custom field : " + val.Name); }
                    }
            }
            else
            {
                // get from persistant object
                foreach (IBIField val in oLigne.FactoryDocumentLigne.InfoLibreFields)
                    if (val.Name == name)
                    {
                        try
                        {
                            oLigne.InfoLibre[val.Name] = value;
                        }
                        catch { throw new Exception("Invalid Format Custom field : " + val.Name); }
                    }
            }
        }

    }
}

