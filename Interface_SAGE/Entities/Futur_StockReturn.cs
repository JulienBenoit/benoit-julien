﻿using System;
using System.Collections.Generic;

namespace Interface_SAGE.Entities
{
    public class StockReturn : WebEntitie   // FuturLog item   ORDER
    {
        /*
         * Obtenir l'état du stock
         * Requête
         * Url	    /Product/GetStocks/{merchantCode}/{login}/{ key}/{ productCode}
         * Type     GET
         * En-tête	Accept "application/json"
         * Paramètres	productCode = Code du produit
         *          Si non renseigné, retourne les états des stocks de tous les produits
         * 
         * Réponse
         * HTTP Status	200
         * Corps	    Liste de StockReturn

        */

        public Int32 AvailableQuantity { get; set; }
        public String Code { get; set; }
        public Int32 ReservedQuantity { get; set; }
        public List<BatchStock> BatchesStock { get; set; }
    }

    public class BatchStock : WebEntitie
    {
        public String Number { get; set; }
        public Int32 OnHandQuantity { get; set; }
    }
}