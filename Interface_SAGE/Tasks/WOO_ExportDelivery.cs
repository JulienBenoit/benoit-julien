﻿using System;
using System.Collections.Generic;
using System.Linq;
using Interface_SAGE.Helper;
using Objets100cLib;
using System.Globalization;
using System.IO;
using System.Text;
using Interface_SAGE.Core;
using System.Net.Http;
using System.Threading.Tasks;

namespace Interface_SAGE.Tasks
{
    public class WOO_ExportDelivery
    {
        private MainProgram Core;
        private Boolean Verbose;
        private String FicNameJsonFake;

        private BSCIALApplication100c om_Cial;
        private BSCPTAApplication100c om_Cpta;

        public WOO_ExportDelivery(MainProgram core, String JsonFake = "", Boolean verbose = false)
        {
            Core = core;
            Verbose = verbose;
            FicNameJsonFake = JsonFake;

            // Base Sage 
            om_Cial = Core.baseSage.OM_baseCial;
            om_Cpta = Core.baseSage.OM_baseCpta;
        }
      

        public async Task Export_Delivery()
        {
            // compteur
            int nb_doc = 0;

            List<IBODocumentVente3> List_export = new List<IBODocumentVente3>();
            Boolean Au_moins_un_doc = false;

            // Parcours des lignes de document ventes de livraisons confirmées
            foreach (IBODocumentVenteLigne3 oDocligne in om_Cial.FactoryDocumentLigne.QueryType(DocumentType.DocumentTypeVenteLivraison, DocumentType.DocumentTypeVenteFactureCpta))
            {

                IBODocumentVente3 oDoc = oDocligne.Document as IBODocumentVente3;
                if (oDoc.DO_Type == DocumentType.DocumentTypeVenteLivraison & oDoc.DO_Statut == DocumentStatutType.DocumentStatutTypeSaisie)
                    continue;

                if (Test_champ_Horodatage_VideOuDefaut(oDocligne.InfoLibre[Core.GetParametres.WOO_Libr_Horodatage]) == false)
                    continue;

                if (oDoc.InfoLibre[Core.GetParametres.WOO_DocLibr_Origine_commande] != "WooCommerce")
                    continue;

                // if (Test_champ_Horodatage_VideOuDefaut(oDoc.InfoLibre[Core.GetParametres.WOO_Libr_Horodatage]) == false) 
                // continue;

                if (om_Cial.FactoryDocumentVente.ExistPiece(DocumentType.DocumentTypeVenteCommande, oDocligne.DL_PieceBC) == true)
                    continue;

                if (List_export.Contains(oDoc) == true)
                    continue;


                // Réservation de la pièce et ajout dans la liste d'export
                try
                {
                    oDoc.CouldModified();
                }
                catch
                { Commun.Journalise("Export Livraison", "Le Document " + oDoc.DO_Piece + " est ouvert, il ne pourra pas être exporté."); }

                List_export.Add(oDoc);
            }

            foreach (IBODocumentVente3 odoc in List_export)
            {
                try
                {
                    if (String.IsNullOrEmpty(FicNameJsonFake) == false)
                    {
                        try
                        {
                            String jsondata = "Document statut complet : " + odoc.DO_Ref;
                            File.AppendAllText(FicNameJsonFake, jsondata);
                        }
                        catch (Exception E)
                        {
                            Commun.Journalise("Export statut DOC", "ERREUR écriture du fichier JSON Fake : " + E.Message, Fl_Alerte: !Core.Automate);
                            return;
                        }
                    }
                    else
                    {
                        await Post_completeDoc(odoc.DO_Ref);
                    }

                    foreach (IBODocumentVenteLigne3 oligne in odoc.FactoryDocumentLigne.List)
                    {
                        try
                        {
                            // Mise à jour de l'horodatage sur le document SAGE
                            try
                            { oligne.InfoLibre[Core.GetParametres.WOO_DocLibr_Origine_commande] = DateTime.Now; }
                            catch
                            { oligne.InfoLibre[Core.GetParametres.WOO_DocLibr_Origine_commande] = DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss"); }
                            finally
                            {
                                oligne.Write();
                            }

                        }
                        catch (Exception Ex)
                        {
                            Commun.Journalise("Export Statut Document", "Erreur lors de la mise à jour Horodatage Document " + odoc.DO_Ref + " - " + oligne.Article.AR_Ref + " : " + Ex.Message);
                        }

                    }
                    nb_doc++;
                    odoc.Read(); // Libération ressource document
                }
                catch (Exception E)
                {
                    // Erreur API déjà journalisée
                }             
            }
            Commun.Journalise("Export Livraison", "Nombre de lignes document exportées : " + nb_doc);
        }


        private Boolean Test_champ_Horodatage_VideOuDefaut(object IL_Horodatage)
        {
            if (IL_Horodatage == null)
                return true;

            DateTime date;

            if (IL_Horodatage.GetType() == typeof(String))
            {
                if (DateTime.TryParseExact(IL_Horodatage.ToString(), new string[] { "dd/MM/yyyy", "dd-MM-yyyy", "ddMMyy", "yyyyMMdd" }, CultureInfo.CurrentCulture, DateTimeStyles.None, out date) == false)
                    return true;
            }

            else if (IL_Horodatage.GetType() == typeof(DateTime))
            {
                date = (DateTime)IL_Horodatage;
            }
            else
                return true;

            if (date == new DateTime(1899, 12, 30))
                return true;
            if (date == new DateTime(1753, 01, 01))
                return true;
            if (date == new DateTime(1900, 01, 01))
                return true;

            return false;

        }


        private async Task Post_completeDoc ( String doc_id)
        {

            var baseUrl = Core.GetParametres.WOO_APIURL;
            baseUrl = baseUrl + (baseUrl.EndsWith("/") ? "" : "/");
            var url = "orders/" + doc_id;
            var consumerKey = Core.GetParametres.WOO_APIlogin;  //"ck_bbdda9851fae53456d9fb56fe29a510bddfdd50e";
            var consumerSecret = DataProtection.Unprotect(Core.GetParametres.WOO_APIMdp);  //"cs_a4989bb994a38b39aefde5461930136494218fd9";
           // var version = "wc/v3";

            using (var client = new HttpClient())
            {

                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Add("Authorization", "Basic " + Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes(consumerKey + ":" + consumerSecret)));
                
                client.BaseAddress = new Uri(baseUrl);
                var content = new StringContent("{ status: \"completed\"}"); //new StringContent(JsonConvert.SerializeObject(contentValue), Encoding.UTF8, "application/json");

                var result = await client.PostAsync(url, content);
                if (result.IsSuccessStatusCode == false)
                    Commun.Journalise("Appel POST API KO", "Status : " + result.StatusCode + " / " + result.ReasonPhrase);
                result.EnsureSuccessStatusCode();
            }
        }

    }
}
