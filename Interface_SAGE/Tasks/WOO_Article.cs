﻿using System;
using System.Collections.Generic;
using System.Linq;
using Interface_SAGE.Helper;
using Interface_SAGE.Formats;
using Objets100cLib;
using System.Globalization;
using System.IO;
using Newtonsoft.Json;
using System.Text.RegularExpressions;
using CsvHelper.Configuration;
using CsvHelper;
using System.Text;
using Interface_SAGE.Core;
using Interface_SAGE.Process;
using Interface_SAGE.Entities;
using System.Data;
using System.Data.SqlClient;
using System.Security.Principal;
using System.Security.AccessControl;
using System.Diagnostics;

namespace Interface_SAGE.Tasks
{
    public class WOO_ExportArticle
    {
        private MainProgram Core;
        private Boolean Verbose;
        private DateTime Date_Export;

        private Char Sep;
        private Char Del;

        private BSCIALApplication100c om_Cial;
        private BSCPTAApplication100c om_Cpta;

        public WOO_ExportArticle(MainProgram core, Boolean verbose = false)
        {
            Core = core;
            Verbose = verbose;
            Date_Export = DateTime.Now;

            // Base Sage 
            om_Cial = Core.baseSage.OM_baseCial;
            om_Cpta = Core.baseSage.OM_baseCpta;

        }
        public void Send_File_FTP()
        {
            if (String.IsNullOrEmpty(Core.GetParametres.FTPUri) == true)
                return;

            // Gestion des sous répertoires
            DirectoryInfo pathDir = new DirectoryInfo(Core.GetParametres.WOO_ExportDir);
            if (pathDir.Exists == false)
            {
                Commun.Journalise("Import Document", "Répertoire de travail inexistant ou innaccessible, ABANDON traitement");
                return;
            }
            // Création des répertoires de destination
            DirectoryInfo TraiteDir = pathDir.CreateSubdirectory("TRAITES");

            try
            {
               /* try
                {
                    DirectoryInfo temp = new DirectoryInfo(@"c:\temp");
                    temp.Create();
                    temp.CreateSubdirectory("ftp");

                    ProcessStartInfo startInfo = new ProcessStartInfo();
                    startInfo.FileName = Interface_SAGE.Properties.Settings.Default.WinSCP_EXE;
                    //startInfo.Arguments = @" /log=""C:\temp\WinSCP.log"" /ini=nul /command ""open ftp://waysup:***@78.41.55.50/"" ""lcd """" " + pathDir.FullName + " """""" ""cd /files/GAYA/PRODUCT"" ""put * "" ""exit""";
                    startInfo.Arguments = " /log=\"C:\\temp\\WinSCP.log\" /ini=nul /command \"open ftp://waysup:***@78.41.55.50/\" \"lcd \"\"" + pathDir.FullName + "\"\"\" \"cd /files/GAYA/PRODUCT\" \"put *.csv\" \"exit\"";
                    //startInfo.Arguments = @" /log=""C:\temp\WinSCP.log"" /ini=nul /command ""open ftp://waysup:***@78.41.55.50/"" ""get -delete /files/GAYA/PRODUCT/* c:\temp\ftp\""  ""exit""";

                    try
                    {
                        using (System.Diagnostics.Process exeProcess = System.Diagnostics.Process.Start(startInfo))
                            exeProcess.WaitForExit(20000);
                    }
                    catch (Exception E)
                    {
                        Commun.Journalise("Import_Document", "Erreur Exécution scritp FTP");
                    }


                    foreach (FileInfo f in pathDir.EnumerateFiles("*"))
                    {
                        String dest = f.FullName.Replace(f.DirectoryName, TraiteDir.FullName);
                        f.MoveTo(dest);
                    }

                }
                catch (Exception E)
                {
                    Commun.Journalise("Import_Document", "Erreur de communication FTP : " + E.Message);
                }*/


                FTP ftp = new FTP(Core);
                String local = Core.GetParametres.WOO_ExportDir + "\\*.csv";
                String remote = Core.GetParametres.FTPPathProduct;

                var list = ftp.Put_Files(local,remote);
               Commun.Journalise("Export_Produit", "Appel FTP, Envoi de " + list.Count + " fichiers");
                foreach (String NomFic in list)
                {
                    try
                    {
                        FileInfo FicSend = new FileInfo(NomFic);

                        String destfic = FicSend.FullName.Replace(FicSend.DirectoryName, TraiteDir.FullName);
                        FicSend.MoveTo(destfic);
                    }
                    catch (Exception E)
                    {
                        Commun.Journalise("Export_Produit Document", "Erreur lors de de l'archivage du ficher : " + E.Message);
                    }
                }
            }
            catch (Exception E)
            {
                Commun.Journalise("Export_Produit", "Erreur de communication FTP : " + E.Message);
            }

        }
        public void Export_Article()
        {
            // Charactères du format de fichier
            Sep = Commun.Get_Char(Core.GetParametres.WOO_DataSep);
            Del = Commun.Get_Char(Core.GetParametres.WOO_DataDel);

            // Gestion des sous répertoires
            DirectoryInfo pathDir = new DirectoryInfo(Core.GetParametres.WOO_ExportDir);
            if (pathDir.Exists == false)
            {
                Commun.Journalise("Export article", "Répertoire de travail inexistant ou innaccessible, ABANDON traitement");
                return;
            }

            // Définition du fichier d'exportation
            String FicDest = AppDomain.CurrentDomain.BaseDirectory + @"\" + Core.GetParametres.WOO_DocFicNomPRODUCT;
            FicDest = FicDest.Replace("YYYYMMDD_HHMMSS", Date_Export.ToString("yyyyMMdd_hhmmss"));
            FicDest = FicDest.Replace("AAAAMMJJ_HHMMSS", Date_Export.ToString("yyyyMMdd_hhmmss"));
            FicDest = FicDest.Replace("YYYYMMDD_HHMM", Date_Export.ToString("yyyyMMdd_hhmm"));
            FicDest = FicDest.Replace("AAAAMMJJ_HHMM", Date_Export.ToString("yyyyMMdd_hhmm"));
            FicDest = FicDest.Replace("YYYYMMDD", Date_Export.ToString("yyyyMMdd"));
            FicDest = FicDest.Replace("AAAAMMJJ", Date_Export.ToString("yyyyMMdd"));
            FicDest = FicDest.Replace("YYMMDD", Date_Export.ToString("yyMMdd"));
            FicDest = FicDest.Replace("AAMMJJ", Date_Export.ToString("yyMMdd"));
            FicDest = FicDest.Replace("*", Date_Export.ToString("yyyyMMdd_hhmmss"));

            // Catégorie tarifaire prix public
            if (om_Cial.FactoryCategorieTarif.ExistIntitule(Core.GetParametres.WOO_StatTarif1) == false)
            {
                Commun.Journalise("Export article", "La catégorie tarifaire pour calcul Prix Public non trouvée, ABANDON traitement");
                return;
            }
            IBPCategorieTarif catTar = om_Cial.FactoryCategorieTarif.ReadIntitule(Core.GetParametres.WOO_StatTarif1) as IBPCategorieTarif;

            // Définition du fichier d'exportation
            FileInfo fic = new FileInfo(FicDest);
            FileStream FS = fic.OpenWrite(); ;

            Boolean Au_moins_une_ligne = false;
            using (StreamWriter sr = new StreamWriter(FS))
            {

                // Génération de la ligne d'entête
                if (Core.GetParametres.WOO_Ligne_entete == true)
                {
                    StringBuilder ligneEntetes = new StringBuilder();
                    ligneEntetes.Append("Reference");
                    ligneEntetes.Append("Designation");
                    ligneEntetes.Append("Design_Long");
                    ligneEntetes.Append("Liste_Bundle");
                    ligneEntetes.Append("Famille");
                    ligneEntetes.Append("PU_public");
                    ligneEntetes.Append("Taux");
                    sr.WriteLine(ligneEntetes);
                    ligneEntetes.Clear();
                }

                // Parcours de la liste de la base article
                foreach (IBOArticle3 oArt in om_Cial.FactoryArticle.List)
                {
                    if (oArt.AR_Publie == false)
                        continue;
                    try
                    {

                        StringBuilder SB = new StringBuilder();

                        // Confection de la ligne
                        Ligne(ref SB, oArt.AR_Ref);
                        Ligne(ref SB, oArt.AR_Design);

                        // Glossaire
                        StringBuilder desclg = new StringBuilder();
                        foreach (IBOGlossaire2 glos in oArt.FactoryArticleGlossaire.List)
                            desclg.Append(glos.GL_Text.Replace(Environment.NewLine,"§§") + " ");
                        Ligne(ref SB, desclg.ToString());

                        Ligne(ref SB, Futur_ProductSend.Get_ListBundle(oArt));                  
                        Ligne(ref SB, oArt.Famille.FA_Intitule);
                        Ligne(ref SB, Futur_ProductSend.Get_Prix_Unitaire( oArt, catTar));
                        Ligne(ref SB, Futur_ProductSend.Get_TauxTVA(oArt));

                        Au_moins_une_ligne = true;
                        sr.WriteLine(SB);
                        SB.Clear();
                        sr.Flush();
                    }
                    catch (Exception E)
                    { Commun.Journalise("Export Article", "ERREUR lors de l'export article " + oArt.AR_Ref + " : " + E.Message); }

                }
                sr.Close();
            }
           

            // si aucun résultat, effacement du fichier
            if (Au_moins_une_ligne == false)
            {
                File.Delete(FicDest);
            }
            else
            {
                try
                {
                    // Déplacement répertoire de destination
                    String destfinal = FicDest.Replace(System.IO.Path.GetDirectoryName(FicDest), pathDir.FullName);
                    File.Move(FicDest, destfinal);

                    // Full acces sur le fichier -> outrepasse les droits d'accès au répertoire hérités
                    FileInfo newFic = new FileInfo(destfinal);
                    FileSecurity fSecurity = newFic.GetAccessControl();
                    var everyone = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
                    fSecurity.AddAccessRule(new FileSystemAccessRule(everyone, FileSystemRights.FullControl, AccessControlType.Allow));
                    newFic.SetAccessControl(fSecurity);

                    Commun.Journalise("Exportation_Referentiel", "Fichier d'export créer : " + destfinal);
                }
                catch (Exception e)
                { Commun.Journalise("EXPORT Traitement fichier", "Erreur lors du déplacement de fichier : " + e.Message); }
            }

        }





        private void Ligne(ref StringBuilder SB, object Texte, int format = 0)
        {
            // Charactères du format de fichier
            //String Sep = Commun.Get_Char(Core.Parametres["Sep"]).ToString();
            //String Del = Commun.Get_Char(Core.Parametres["Del"]).ToString();

            switch (format)
            {
                case 1: // entier
                    SB.Append(Del);
                    SB.AppendFormat("{0,12:N0}", Texte);
                    SB.Append(Del + Sep);
                    break;
                case 2: // numerique
                    SB.Append(Del);
                    SB.AppendFormat("{0,12:N2}", Texte);
                    SB.Append(Del + Sep);
                    break;
                case 3: // Date
                    SB.Append(Del + ((DateTime)Texte).ToString("dd/MM/yyyy") + Del + Sep);
                    break;
                case 0:
                default:  // Chaine de caractères 0 ou defaut 
                          //SB.Append(Del + Texte + Del + Sep);
                    SB.Append(Del + Texte.ToString().Replace("\"", "'") + Del + Sep);//.Trim(new Char[] { (Char)10, (Char)13, (Char)9 }) + Del + Sep);
                    break;
            }
        }
    }
}
